# Particle Finite Volume Method (PFVM)
PFEM-2 based solvers implemented in OpenFOAM

## Requeriments

### OpenFOAM-6
To install, follow:
https://openfoam.org/download/6-ubuntu/

or, for newer Linux distributions:
https://openfoam.org/download/6-source/

newer OpenFOAM's versions are not currently tested

## Installation
Execute:
`$ ./Allwmake`
to compile the library, solvers and utilities.

## Tutorial cases
To run the tutorials, go to the case folder
- tutorials/particlePimpleFoam/TaylorGreenVortex2D_PFVM1stOrder
- tutorials/particlePimpleFoam/TaylorGreenVortex2D_PFVM2ndOrder

and execute:
`$ ./Allrun`


